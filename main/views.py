from django.shortcuts import render, get_object_or_404, redirect
from .models import RumahSakit, Volunteer
from .forms import FormRumahSakit, FormVolunteer
from django.http import HttpResponseRedirect

def home(request):
    tRumahSakit = RumahSakit.objects.all()
    context = {
        "rumahsakit" : tRumahSakit
    }
    return render(request, 'main/home.html', context)

def rumahsakit(request):
    form = FormRumahSakit()

    if request.method == "POST":
        form = FormRumahSakit(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect("/")
    
    context = {
        "form" : form,
    }
    return render(request, 'main/tambahRS.html', context)

def volunteer(request, *args, **kwargs):
    form = FormVolunteer()

    if request.method == "POST":
        nama_Volunteer = request.POST.get("nama_Volunteer")
        KTP = request.POST.get("KTP")
        alasan = request.POST.get("alasan")
        volunteerDaftar = Volunteer.objects.create(nama_Volunteer = nama_Volunteer, KTP = KTP, alasan = alasan)
        rumahsakit = RumahSakit.objects.get(id = kwargs["id"])
        rumahsakit.volunteer.add(volunteerDaftar)
        return redirect("/")
    
    
    context = {
        "form" : form,
    }

    return render(request, "main/tambahVolunteer.html", context)