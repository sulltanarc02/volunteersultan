from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambah-rumah-sakit/', views.rumahsakit, name='rumahsakit'),
    path('tambah-volunteer/<int:id>', views.volunteer, name='volunteer'),
]
