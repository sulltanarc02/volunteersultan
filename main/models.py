from django.db import models
from django.urls import reverse

# Create your models here.
class Volunteer(models.Model):
    nama_Volunteer = models.CharField(max_length=100)
    KTP = models.CharField(max_length=100)
    alasan = models.TextField()
    class Meta:
        ordering = ['nama_Volunteer']

class RumahSakit(models.Model):
    nama_Rumah_Sakit = models.CharField(max_length=100)
    alamat = models.CharField(max_length=300)
    telepon = models.CharField(max_length=100)
    gambar = models.ImageField(upload_to='images/', null=True, blank=True)
    volunteer = models.ManyToManyField(Volunteer)
    class Meta:
        ordering = ['nama_Rumah_Sakit']

    def absolute_url(self):
        return reverse("main:volunteer", kwargs = {"id" : self.id})